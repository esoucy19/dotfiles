# dotfiles
dotfiles for unix programs, managed with GNU Stow

# Arch Openbox installation
These are the steps I recorded for my Arch Linux setup with Openbox.

## initial config

### initial pacstrap
TODO: remove virtualbox-guest-utils from pacstrap when done testing in virtualbox
pacstrap /mnt base base-devel xorg xorg-xinit openbox pyxdg libnotify  networkmanager grub compton virtualbox-guest-utils vim xterm xdg-utils ntp

## system config
### generate fstab
genfstab -U /mnt >> /mnt/etc/fstab

### chroot to new system
arch-chroot /mnt

### set time zone
ln -sf /usr/share/zoneinfo/Armerica/Toronto /etc/localtime
hwclock --systohc

### set locale
vim /etc/locale.gen <!-- uncomment en_CA.UTF-8 and en_US.UTF-8 -->
locale-gen
localectl --set-locale en_CA.utf8

### set keyboard layout
localectl set-keymap cf

### hostname and hosts file
echo "myhostname" > /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1 localhost" >> /etc/hosts
echo "127.0.0.1 myhostname.localdomain myhostname" >> /etc/hosts

### enable services
systemctl enable NetworkManager.service
systemctl enable ntpd.service

## uncomment enable wheel group in visudo
visudo

## set root password
passwd

## create my user account
useradd -m -G wheel -s /bin/bash esoucy
passwd esoucy

## install grub
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg

## exit unmount and reboot
exit
umount -R /mnt
reboot

# Arch post install config
### install packages
pacman -Syu ttf-fira-sans adobe-source-code-pro-fonts stow git tint2 feh dbus-python xbindkeys dmenu emacs gnome-alsa-mixer

### install yaourt

### install obmenu-generator
yaourt -Syu obmenu-generator quicktile-git pnmixer --aur

