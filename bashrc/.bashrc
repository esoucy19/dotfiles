#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export PATH=$HOME/.rbenv/bin:$PATH

export ALTERNATE_EDITOR=""
export EDITOR="vim"
export VISUAL="vim"

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '


# Python virtualenv settings
# Disable virtualenv prompt
export VIRTUAL_ENV_DISABLE_PROMPT=true
# Activate default env
source ~/python/bin/activate 2> /dev/null

# Ruby rbenv settings
eval "$(rbenv init -)" 2> /dev/null

